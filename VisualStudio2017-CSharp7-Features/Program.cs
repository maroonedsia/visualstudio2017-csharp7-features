﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    class Program
    {
        static void Main(string[] args)
        {
            // 01: test out
            //CSharp7.PrintCoordinates();

            // 02: test patterns
            //CSharp7.PrintStars(1.02);
            //CSharp7.PrintStars("Hello");
            //CSharp7.PrintStars(12);

            // 03: test tuples:
            //var name = CSharp7.LookupName(5);
            //Console.WriteLine($"Your name is \"{name.Item1} {name.Item2} {name.Item3}\"");

            //var name2 = CSharp7.LookupName2(5);
            //ValueTuple<string, string, string> name3 = name2;
            //Console.WriteLine($"Your name is \"{name.Item1} {name3.Item2} {name3.Item3}\"");

            // 07: test ref return
            CSharp7.Literals();
            CSharp7.TestFind();



        }
    }
}
