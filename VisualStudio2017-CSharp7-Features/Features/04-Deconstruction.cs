﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        // A deconstructing declaration is a syntax for splitting a tuple(or other value) 
        // into its parts and assigning those parts individually to fresh variables

        public static void DeconstructionTest()
        {
            // 1:
            (string first, string middle, string last) = LookupName(10);

            // 2: using var
            (var first2, var middle2, var last2) = LookupName(12); // var inside

            // 3: use a single var:
            var (first3, middle3, last3) = LookupName(14);

            // 4: deconstruct into existing variables
            string f, m, l;
            (f, m, l) = LookupName(16);

            // 5: for any type that provides a "Deconstruct" method
            var p = new Point(3, 2);
            (double X, double Y) = p;
        }
    }

    public class Point
    {
        public int X { get; }
        public int Y { get; }

        public Point(int x, int y) { X = x; Y = y; }

        // Here is Deconstruct:
        // Notice out arguments
        public void Deconstruct(out int x, out int y)
        {
            x = X;
            y = Y;
        }
    }
}
