﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        public class Person2
        {
            public string Name { get; }

            public Person2(string name) => Name = name ?? throw new ArgumentNullException(nameof(name));

            public string GetFirstName()
            {
                var parts = Name.Split(' ');
                return (parts.Length > 0) ? parts[0] : throw new InvalidOperationException("No name!");
            }
            public string GetLastName() => throw new NotImplementedException();
        }
    }
}
