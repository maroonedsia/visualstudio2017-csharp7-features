﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        public static int Fibonacci(int x)
        {
            if (x < 0) throw new ArgumentException("Less negativity please!", nameof(x));

            int preDummy = 123;
            return Fib(x).current;
            int postDummy = 123;
            
            (int current, int previous) Fib(int i)
            {
                // main method local variables are accessible here
                int dummyCopy = preDummy;

                // this will throw a compile error, because it's declared after method call:
                // dummyCopy = postDummy;

                if (i == 0) return (1, 0);
                var (p, pp) = Fib(i - 1);
                return (p + pp, p);
            }
        }
    }
}
