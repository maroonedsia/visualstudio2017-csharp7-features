﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        //Patterns are syntactic elements that can test that a value has a certain "shape",
        // and extract information from the value when it does

        //This is just the beginning – patterns are a new kind of language element in C#, 
        // and we expect to add more of them to C# in the future.

        public static void PrintStars(object o)
        {
            if (o is null) return;     // constant pattern "null"

            if (!(o is int i)) return; // type pattern "int i"

            // Note: like "out" variables, pattern variables are mutable.
            // We often refer to out variables and pattern variables jointly as "expression variables".

            Console.WriteLine(new string('*', i));
        }

        public static void PrintShapes(Shape shape)
        {
            switch (shape)
            {
                case Circle c:
                    Console.WriteLine($"circle with radius {c.Radius}");
                    break;
                case Rectangle s when (s.Length == s.Height):
                    Console.WriteLine($"{s.Length} x {s.Height} square");
                    break;
                case Rectangle r:
                    Console.WriteLine($"{r.Length} x {r.Height} rectangle");
                    break;
                default:
                    Console.WriteLine("<unknown shape>");
                    break;
                case null:
                    throw new ArgumentNullException(nameof(shape));
            }

            // Notes:
            // 1. Now, the order of case clauses now matters
            // 2. The default clause is always evaluated last: Even though the null case above comes last.
            // 3. The null clause at the end is not unreachable.
            // 4. Pattern variables are in scope only in the corresponding switch section
        }
    }

    public class Shape
    {
    }

    internal class Rectangle: Shape
    {
        public float Length { get; set; }
        public float Height { get; set; }
    }

    internal class Circle: Shape
    {
        public float Radius { get; set; }
    }
}
