﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        class Person
        {
            private static Dictionary<int, string> names = new Dictionary<int, string>();

            // constructors
            public Person(int id, string name) => names.Add(id, name);


            public int Id { get; set; }

            // accessors
            public string Name
            {
                get => names[Id];                                 // getters
                set => names[Id] = value;                         // setters
            }

            // destructors
            ~Person() => names.Remove(Id);
        }
    }
}
