﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        // A System.Tuple<...> existed previously

        // C# 7:
        // You need to install "System.ValueTuple" Nuget package
        public static (string, string, string) LookupName(long id) // tuple return type
        {
            string last = "Mortazavi";
            string middle = "2nd";
            return ("Sia", middle, last); // tuple literal
        }

        public static (string First, string Middle, string Last) LookupName2(long id) // tuple return type
        {
            string last = "Mortazavi";
            string middle = "2nd";
            return ("Sia", middle, last); // tuple literal
        }

        // Note: 2 tuples with same items types and item values are equal
        // AND WILL HAVE THE SAME HASH CODE
        // This makes tuples useful for many other situations beyond multiple return values.
        // For instance, if you need a dictionary with multiple keys, 
        // use a tuple as your key and everything works out right.
        // If you need a list with multiple values at each position, 
        // use a tuple, and searching the list etc.will work correctly.
    }
}
