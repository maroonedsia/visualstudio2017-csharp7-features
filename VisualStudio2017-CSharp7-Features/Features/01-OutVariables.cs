﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        public static void PrintCoordinatesOld()
        {
            int x, y; // have to "predeclare"
            OutHelper(out x, out y);
            Console.WriteLine($"({x}, {y})");
        }

        public static void PrintCoordinates()
        {
            OutHelper(out int x, out int y);

            // nce the out variables are declared directly as arguments to out parameters,
            // the compiler can usually tell what their type should be (unless there are conflicting overloads),
            // so it is fine to use var instead of a type to declare them:
            OutHelper(out var i, out var j);
            Console.WriteLine($"i = {i}, j = {j}");

            // a good common use:
            string s = "4324";
            if (int.TryParse(s, out var number))
            {
                Console.WriteLine($"number = {number}");

            }
        }

        private static void OutHelper(out int x,out int y)
        {
            x = 121;
            y = 999;
        }
    }
}
