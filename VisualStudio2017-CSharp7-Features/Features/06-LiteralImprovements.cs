﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualStudio2017_CSharp7_Features
{
    public partial class CSharp7
    {
        public static void Literals()
        {
            // digit separator
            var d = 123_456; // is just equal 123456
            var x = 0xAB_CD_EF;

            // also in decimal value:
            double pie = 3.141_592_653;

            // it doesn't matter where you put the '_':
            var ssn = 12_345_7890;
            var phoneNumber = 123_123_1234;

            // binary literals (WoOOoo)
            var b = 0b101010001;
            b = 0b0000_0101_1101_1111;
        }
    }
}
