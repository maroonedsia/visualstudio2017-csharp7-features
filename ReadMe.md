# C# 7 & Visual Studio 2017 New Features

### References
- [Official Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/articles/csharp/csharp-7)
- [MSDN Blog](https://blogs.msdn.microsoft.com/dotnet/2017/03/09/new-features-in-c-7-0/)
- [C# 7 Features Cheat Sheet](https://github.com/alugili/CSharp7Features/blob/master/C%237CheatSheet.pdf)

